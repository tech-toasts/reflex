---
description: >-
  This page serves as a way to showcase the external views and objects created
  along the way for the sake of this project
---

# Project Management Links

## LucidChart

This section deals with showcasing the ERD and the Class Diagrams for **Reflex** by using the tool, **LucidChart**. The diagrams can be seen at this [_link_](https://lucid.app/lucidchart/invitations/accept/inv\_3e5c74b1-ccb3-4715-b2f7-40caaf485f86?viewport\_loc=-576%2C-130%2C3328%2C1688%2C0\_0)\_\_

## Figma

The overall frontend prototype for the page can be viewed at Figma at this [_link_](https://www.figma.com/file/PKzIocgkOTkDdy2mtKyk9c/Reflex?node-id=0%3A1)\_\_

## Notion

The notion page for this project can be viewed [_here_](https://www.notion.so/Reflex-bfe33ff4e9984072aeac514fe6d6bb30)\_\_

## GitHub

The GitHub, of course, is at this [_link_](https://github.com/Rubix982/Reflex)\_\_

## Test Cases

The test cases written for this project can be found in this [excel sheet](https://docs.google.com/spreadsheets/d/1IoFYTfmonSmj8XrJVA4r6KDa8BZbtHO\_/edit?usp=sharing\&ouid=106105819354897704806\&rtpof=true\&sd=true).

## Documentation - Assignments

Some of the project's work was covered as assignment work and is shared under the `docs` folder on the root location of the GitHub repository.
