# Building

To build,

```
make build
```

To start the containers,

```
make up
```

To remove the containers,

```
make remove
```
