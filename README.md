---
description: Hello, reader!
---

# Welcome page

## About The Project

Reflex is a small imitation of the Flex Student application at FAST NUCES @ Karachi, Main Karachi. At the moment, it simply tries to emulate the process of marking attendance for students in classrooms. It serves to be a prototype for the application right now.

## Getting Started

To get started, you can check out the,

{% content-ref url="project-structure.md" %}
[project-structure.md](project-structure.md)
{% endcontent-ref %}

{% content-ref url="system-requirements.md" %}
[system-requirements.md](system-requirements.md)
{% endcontent-ref %}

{% content-ref url="building.md" %}
[building.md](building.md)
{% endcontent-ref %}

## Usage

For an in-depth article, check out the below page,

{% content-ref url="how-to-use.md" %}
[how-to-use.md](how-to-use.md)
{% endcontent-ref %}

## Roadmap

Check out the Notion page under the following page for more details about the RoadMap,

{% content-ref url="project-management-links.md" %}
[project-management-links.md](project-management-links.md)
{% endcontent-ref %}

## Contributing

See GitHub repository for contribution advice.

## License

See GitHub repository for the license agreement.

## Contact

Please contact the following for any support, bug reports, or any helpful advice you may have for this project. Thank you.

* Saif Ul Islam, [https://github.com/rubix982](https://github.com/rubix982)
* Muhammad Anas Aziz Siddiqui, [https://github.com/sidanas22](https://github.com/sidanas22)
* Muhammad Taha, [https://github.com/i-Taha](https://github.com/i-Taha)
